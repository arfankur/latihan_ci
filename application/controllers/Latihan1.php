<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Latihan1 extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
     $this->load->model("Latihan1Model");
    }
    
    public function index()
    {
        $this->load->view('input');
    }
    public function proses()
    {
        $nilai1 = $this->input->post('nilai1');
        $nilai2 = $this->input->post('nilai2');
        echo $nilai1."<br>";
        echo $nilai2."<br>";
        $hasil = $this->Latihan1Model->jumlahkanDuaAngka($nilai1,$nilai2);
        echo "hasil dari $nilai1 + $nilai2 adalah $hasil" ;
    }
}