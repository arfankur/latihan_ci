<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MakeFullName extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('MakeFullNameModel');
    }
    
    public function index(){
        $this->load->view('makefullname');
    }
    public function makeFullName(){
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        echo $this->MakeFullNameModel->makeFullName($first_name,$last_name);
    }
}