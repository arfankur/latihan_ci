<?php
class MakeFullNameModel extends CI_Model{

    public $first_name;
    public $last_name;

    public function makeFullName($first_name, $last_name){
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        return $this->first_name.' '.$this->last_name;
    }

}